# Development Basic Setup

## git - source code management

### Setup
1. Download the portable git from https://git-scm.com/download
2. Extract the downloaded zip file.
3. Set the PATH on console.

To verify run the setup
```sh
$ java -version
```

### Configure
1. Add git user email <code>git config --global user.email "\<email\>"</code>
2. Add git user name <code>git config --global user.name "\<name\>"</code>

## maven - build automation

### Setup
1. Downlaod the maven binary version from https://maven.apache.org/download.cgi
2. Extract the downloaded zip file.
3. Set the PATH on console.

To verify run the setup
```sh
$ mvn -v
```

## Notes
For windows we can use batch files to configure the PATH variable.

Ref : [console.bat](console.bat)