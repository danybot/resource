# Create new maven project

## gitlab

1. Login to [gitlab](https://gitlab.com/)

2. Create New project [[Link](https://gitlab.com/projects/new)]
    - Name of the project should be hyphen seperated <code>my-sample-project</code> .
    - Include the ReadMe.md file and add description about the project(use [Description] tag).

3. After creating add .gitignore file to remove the unwanted files to commit.
    - clicke <a>Create new</a> button and select <a>new file</a>.
    - change the type to .gitignore and you can specify the files and folder that need to be ignored.
    - you can use [gitignore](https://www.gitignore.io/) to create the .gitignore file and mention it in commit.

## eclipse

1. Clone the project into the workspace

2. Open the eclipse and create new maven project
    - Latest version of maven won't support 1.5 build version so  change the version to 1.8<latest>. 

    <pre><code>&lt;properties&gt;
        &lt;maven.compiler.source&gt;1.8&lt;/maven.compiler.source&gt;
        &lt;maven.compiler.target&gt;1.8&lt;/maven.compiler.target&gt;
    &lt;/properties&gt;
    </code></pre>

3. Initialize the git
    - <code>cd project-name</code>.
    - Initialize the git <code>git init</code>
    - Initialize origin <code>git remote add origin \<remote\></code>
    - Pull the files if any <code>git pull \<remote\> \<branch\></code>
    - Add the files using <code>git add</code>
    - Commit the files <code>git commit -m "commit message"</code>
    - On initial push use <code>git push --set-upstream origin master<code>