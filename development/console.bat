:: stop the echo on console
@echo off

:: constants that need often. can be accessed by %CONSTANTS%
:: binaries of different programs are maintained in Apps -> Windows -> <program_name> -> <optional_binary_path>
set pwd=%~dp0
set windowsApps=%pwd%Apps\Windows

:: Apps
set PATH=%PATH%;%windowsApps%\Cmder
set PATH=%PATH%;%windowsApps%\jdk\bin
set PATH=%PATH%;%windowsApps%\eclipse
set PATH=%PATH%;%windowsApps%\apache-maven-3.5.4\bin

start Cmder