# Resource

Resource : contains necessary document for complete reference.

# Index

> development
>> - [console](development/console.bat)
>> - [env-setup](development/env-setup.md)
>> - [project-setup](development/project-setup.md)